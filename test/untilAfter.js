import test from 'ava'
import {untilAfter} from '..'

test('chop after text', (t) => {
  t.is('foobar'::untilAfter('foo'), 'foo')
})

test('chop after emoji', (t) => {
  t.is('foo💩bar'::untilAfter('💩'), 'foo💩')
})
